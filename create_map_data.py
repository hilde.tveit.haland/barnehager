from bs4 import BeautifulSoup as bs
import requests
import pandas as pd
import numpy as np
import datetime
import re


# #### Set URL:

# In[2]:


url = 'https://www.oslo.kommune.no/barnehage/ledige-barnehageplasser/#gref'


# #### Get soup:

# In[3]:


def get_soup(url, check=True):
    res = requests.get(url)
    
    if check==True:
        if res.status_code != 200:
            raise ValueError('Status code for given URL {}. Make sure web address can be scraped. \n            Force scrape by setting check=False.'.format(res.status_code))

    return bs(res.content, 'html.parser')


# In[4]:


def get_soup_items(soup):
    soup_items = []

    for i in str(soup).split('data-cpwysiwygtableid="')[1:]:
        soup_items.append(i[:10].split('"')[0])
        
    return soup_items


# #### Extract table with barnehager for a specific area:

# In[5]:


def extract_table(name, etype, soup, soup_item):
    return soup.find(etype, {'class': name, 'data-cpwysiwygtableid':soup_item})


# In[6]:


name = 'ctckeditortable headertype-col articletemplate-8'
soup_item = 912
etype = 'table'

# tabell = extract_table(name, etype, soup, soup_item)
tabell = extract_table(name, etype, get_soup(url, check=True), get_soup_items(get_soup(url, check=True))[1])


# #### Get dataframe of table:

# In[7]:


def create_plot_table(tabell):
    link = []; smaabarn = []; storbarn = []; maaned = []
    
    for i in range(len([x.split('<td>') for x in str(tabell).split('href=') if x[:4] == '"htt'])):

        link.append(re.findall('"([^"]*)"', str([x.split('<td>') for x in str(tabell).split('href=') if x[:4] == '"htt'][i][0])))
        if len([x.split('<td>') for x in str(tabell).split('href=') if x[:4] == '"htt'][i]) > 1:
            smaabarn.append(re.findall(r"(\d+)<", str([x.split('<td>') for x in str(tabell).split('href=') if x[:4] == '"htt'][i][1])))
            storbarn.append(re.findall(r"(\d+)<", str([x.split('<td>') for x in str(tabell).split('href=') if x[:4] == '"htt'][i][2])))            
            if len(re.findall('^.*?(?=<)', str([x.split('<td>') for x in str(tabell).split('href=') if x[:4] == '"htt'][i][3])))> 1:
                maaned.append([re.findall('^.*?(?=<)', str([x.split('<td>') for x in str(tabell).split('href=') if x[:4] == '"htt'][i][3]))[1].split('>')[1]])
            else:
                maaned.append(re.findall('^.*?(?=<)', str([x.split('<td>') for x in str(tabell).split('href=') if x[:4] == '"htt'][i][3])))
        else:
            smaabarn.append([0])
            storbarn.append([0])
            maaned.append(['-'])
           
    temp = pd.DataFrame([link, smaabarn, storbarn, maaned]).T   
    
    for i in temp.columns:
        temp[i] = temp[i].apply(lambda x: x[0] if len(x)>0 else 0)
    
    temp.columns = ['Link','Smaabarn','Storbarn','Maaned']
    
#     temp['bydel'] = re.search(r'<bydel >(.*?)</caption>', str(tabell)).group(1)
        
    return temp


# #### Get location for each barnehage:

# In[11]:


def get_map_data(url):
    
    soup = get_soup(url, check=True)
    
    lat = re.search('data-latitude="(.*)" data-longitude', str(soup))
    lon = re.search('data-longitude="(.*)" data-name', str(soup))
    name = re.search('data-name="(.*)" data-zoom', str(soup))

    return lat.group(1), lon.group(1), name.group(1)


# In[13]:


def make_lat_lon_name_cols(df):
    
    if 'map_data' not in df.columns:
        raise ValueError('Map_data column not in input dataframe, run get_map_data function.')
    
    df['Lat'] = df['map_data'].apply(lambda x: float(x[0]))
    df['Lon'] = df['map_data'].apply(lambda x: float(x[1]))
    df['Name'] = df['map_data'].apply(lambda x: x[2])
    
    return df[['Name', 'Smaabarn', 'Storbarn', 'Maaned', 'Lat', 'Lon', 'Link']]
    


# ### Edit columns:

# In[86]:


from pyjarowinkler import distance


# In[87]:


months = ['Januar','Februar','Mars','April','Mai','Juni','Juli','August','September','Oktober','November','Desember']


# In[137]:


def get_norm_month(word):  
    if type(word) == str and word != '': 
        match = 0; month = 'Januar'

        for i in range(12):
            if distance.get_jaro_distance(months[i],  word, winkler=True, scaling=0.1) > match:
                match = distance.get_jaro_distance(months[i],  word, winkler=True, scaling=0.1)
                month = months[i]
        return month
    else:
        return np.nan


# In[114]:


def change_name(x):
 
    if len(x.split('-')) > 1:
        name = ''
        for part in x.split('-'):
            name = name + '-' + part[0].upper() + part[1:]
        name = name[1:]
    else:
        name = x[0].upper() + x[1:]

    return name


# ### Extract data for all areas:

# In[89]:


barnehager = pd.DataFrame(columns=['Name', 'Smaabarn', 'Storbarn', 'Maaned', 'Lat', 'Lon', 'Link', 'Bydel'])


# In[90]:


url = 'https://www.oslo.kommune.no/barnehage/ledige-barnehageplasser/#gref'


# In[91]:


soup = get_soup(url, check=True)


# In[92]:


soup_items = []

for i in str(soup).split('data-cpwysiwygtableid="')[1:]:
    soup_items.append(i[:10].split('"')[0])


# In[93]:


name = 'ctckeditortable headertype-col articletemplate-8'
etype = 'table'


for item in soup_items:
    tabell = extract_table(name, etype, soup, item)
    
    df = create_plot_table(tabell)
    
    df['map_data'] = df['Link'].apply(lambda x: get_map_data(x))
    
    df = make_lat_lon_name_cols(df)
    
    barnehager = barnehager.append(df, sort=False)


# In[94]:


barnehager['Bydel'] = barnehager['Link'].apply(lambda x: change_name(re.search(u'bydel.(.*?)/', x).group(1)))


# In[118]:


barnehager.reset_index(inplace=True)


# In[139]:


barnehager['Maaned'] = barnehager['Maaned'].apply(lambda x: get_norm_month(x))


# In[ ]:


barnehager.to_csv('barnehager_oppdater_{}.csv'.format(str(datetime.datetime.now())[:10]))


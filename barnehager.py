    
import streamlit as st
import datetime
import os
import pandas as pd



max_date = max([x[-14:-4] for  x in os.listdir() if 'barnehager_oppdatert_' and '.csv' in x])

while datetime.datetime.now().replace(hour=0, minute=0, second=0, microsecond=0) >= datetime.datetime.strptime(max_date, '%Y-%m-%d') is False:
    execfile('create_map_data.py')
    
    

barnehager = pd.read_csv('C:/Users/hildeha/Dev/FUNS/streamlit/barnehager_oppdater_' + str(max_date) + '.csv')
barnehager.rename(columns={'Lat':'lat','Lon':'lon'}, inplace=True)

st.markdown('# Oversikt over ledige barnehageplasser i Oslo')

st.sidebar.markdown("# Filtrer pa bydel og ledige plasser")

alle = list(barnehager['Bydel'].unique())
option = st.sidebar.selectbox(
    'Bydel',
     ['Alle']+list(barnehager['Bydel'].unique()))

alle_m = list(barnehager['Maaned'].unique())
option_m = st.sidebar.selectbox(
    'Maaned',
     ['Alle']+list(barnehager['Maaned'].unique()))


smb = st.sidebar.slider('Ledige plasser smaabarn:', 0, max(barnehager['Smaabarn']), 1) 
stb = st.sidebar.slider('Ledige plasser storbarn:', 0, max(barnehager['Storbarn']), 0)


if option == 'Alle': 
    bdels = alle
else:
    bdels = [option]
    
if option_m == 'Alle': 
    ms = alle_m
else:
    ms = [option_m]  

    
filtered_data = barnehager.loc[ (barnehager['Bydel'].isin(bdels)) & (barnehager['Maaned'].isin(ms)) & (barnehager['Smaabarn'] >= smb) & (barnehager['Storbarn'] >= stb)]


st.write(filtered_data[list(barnehager.columns[2:6])]) 
st.map(filtered_data[['lat','lon']], zoom=10)

for e in filtered_data['Link']:
    st.write(e)


